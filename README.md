
# Kungfoo Simple Math Equation Parser

This module is intended to parse and eval safely simple math equations.

Available operands are:

- \^ power
- \% modulus
- \* multiplication
- \/ division
- \- substraction
- \+ addition


Based on https://stackoverflow.com/a/13056137/1289584

===

Simply put this file in your dir and import it:

from mathparser import Equation

Then the basic equation is as follows:

```
exp='1000  * 1.15'
o = Equation(exp)
print(o.eval())
```

Also you can add parentheses:

```
exp='1000  * (1.15 + 2)'
o = Equation(exp)
print(o.eval())
```

or

```
exp='1000  * (1.15 + ( 2 * 5 / 2.5))'
o = Equation(exp)
print(o.eval())
```

The class has a few constants defined, you can see them as follows:

```
print(o.constants)
```

Also you can add new ones:

```
o.constants = {'radius': 2}
```

...if the constant exists it will be overwritten.

So you can use those constants:

```
exp='pi*radio^2'
o = Equation(exp)
o.constants = {'radio': 2}
print(o.eval())
```

The class comes with logger, so if you set looger on some more info will be displayed:

```
logger.setLevel(logging.DEBUG)
```
