#!/usr/bin/env python3

"""

Kungfoo Simple Math Equation Parser

This module is intended to parse simple math equations.
Available operands are:
^ power
% modulus
* multiplication
/ division
- substraction
+ addition


Based on https://stackoverflow.com/a/13056137/1289584

===

Simply put this file in your dir and import it:

from mathparser import Equation

Then the basic equation is as follows:

exp='1000  * 1.15'
o = Equation(exp)
print(o.eval())

Also you can add parentheses:

exp='1000  * (1.15 + 2)'
o = Equation(exp)
print(o.eval())

or

exp='1000  * (1.15 + ( 2 * 5 / 2.5))'
o = Equation(exp)
print(o.eval())

The class has a few constants defined, you can see them as follows:

print(o.constants)

Also you can add new ones:

o.constants = {'radius': 2}

...if the constant exists it will be overwritten.

So you can use those constants:

exp='pi*radio^2'
o = Equation(exp)
o.constants = {'radio': 2}
print(o.eval())

The class comes with logger, so if you set looger on some more info will be displayed:

logger.setLevel(logging.DEBUG)

"""
import operator
import re
import logging
import pandas as pd
import uuid

__author__ = "Juan Kungfoo de la Camara Beovide"
__license__ = "Creative Commons Attribution-Noncommercial-Share Alike license"
__version__ = "0.0.8-dev"
__maintainer__ = "Juan Kungfoo de la Camara Beovide"
__email__ = "juanmatias@gmail.com"

# logging
LOG_FORMAT="%(asctime)s:%(levelname)s:%(message)s"
logging.basicConfig(format=LOG_FORMAT)
logger = logging.getLogger('kungfoomathparser')
#logger.setLevel(logging.DEBUG)

class Equation():
    _check_re = r'^([a-zA-z]|[0-9]|[\.\(\)\+\-\*\/\ \^\%])*$'
    _check_final_re = r'^([0-9]|[\.\(\)\+\-\*\/\ \^\%])*$'
    _eval_state = 0
    _constants = {'pi':3.1415,'e': 2.71828}
    _constants_not_number = {}
    _constant_name_re_setter = r'^[a-zA-Z][a-zA-Z0-9\_]+$'
    _constant_value_re_setter = r'^[0-9]*\.{0,1}[0-9]+$'
    _constant_name_re = r'\b[a-zA-Z][a-zA-Z0-9\_]+\b'

    def __init__(self,equation):
        assert self._check_equation(equation), f'Bad equation: {equation}'
        self._equation = equation
        self._equation_temp = ''
        self._result = None
        logger.debug(f'Created object with equation => {self._equation}')

    @property
    def equation(self):
        return self._equation

    @property
    def constants(self):
        return self._constants

    @constants.setter
    def constants(self, constants: dict = {}):
        assert type(constants) == dict, 'Constans is a dict with Cons=>Value pairs'
        res =  [(not re.match(self._constant_name_re_setter,i) is None) and (not re.match(self._constant_value_re_setter,str(constants[i])) is None) for i in constants]
        if False in res:
            raise Exception('Wrong value in constants')
        for ic in constants:
            found = False
            for c in self._constants:
                if c == ic:
                    self._constants[c]=constants[ic]
                    found = True
                    break
            if not found:
                self._constants[ic]=constants[ic]

    @property
    def constants_not_number(self):
        return self._constants_not_number

    @constants_not_number.setter
    def constants_not_number(self, constants: dict = {}):
        assert type(constants) == dict, 'Constans is a dict with Cons=>Value pairs'
        res =  [(type(constants[i]) == pd.core.frame.Series and str(constants[i].dtypes) in ['float64', 'int64']) for i in constants]
        if False in res:
            raise Exception('Wrong value in constants, only Pandas Series are accepted.')
        for ic in constants:
            found = False
            for c in self._constants_not_number:
                if c == ic:
                    self._constants_not_number[c]=constants[ic]
                    found = True
                    break
            if not found:
                self._constants_not_number[ic]=constants[ic]

    def constant_name_re(self):
        return self._constant_name_re


    def _check_equation(self, equation = None):
        return re.match(self._check_re,(self._equation if equation is None else equation))

    def _check_final_equation(self, equation = None):
        logger.debug('_check_final_equation')
        logger.debug(self._equation_temp)
        if len(self._constants_not_number) > 0:
            logger.debug('There are not-numeric constants, doing extended check...')

            groups_re = re.findall(self._constant_name_re,(self._equation_temp if equation is None else equation))
            logger.debug('List of non-numeric constants found:')
            logger.debug(groups_re)
            logger.debug('Checking them...')
            not_found = []
            for const in groups_re:
                if not const in self._constants_not_number.keys():
                    not_found.append(cons)
            logger.debug('Not found list:')
            logger.debug(not_found)
            return len(not_found) == 0
        else:
            logger.debug('There are only numeric constants, doing standard check...')
            return re.match(self._check_final_re,(self._equation_temp if equation is None else equation))


    def eval(self):
        return self._eval()

    def _eval(self):
        self._eval_constants()
        if not self._check_final_equation():
            raise Exception(f'Something went wrong with constants: {self._equation_temp}')
        self._eval_parentheses()
        return self._result

    def _eval_constants(self):
        logger.debug('_eval_constants function')
        local_op = self._equation
        local_op_check = self._equation
        logger.debug(f"lookin {self._constant_name_re} into {local_op}")
        groups_re = re.findall(self._constant_name_re,local_op_check)
        logger.debug(groups_re)
        while len(groups_re)>0:
            for cons in groups_re:
                logger.debug(f' processing => {cons}')
                found = False
                if cons in self._constants.keys():
                    local_op = re.sub(r'\b'+cons+r'\b',str(self._constants[cons]),local_op)
                    local_op_check = re.sub(r'\b'+cons+r'\b',str(self._constants[cons]),local_op_check)
                    found = True
                    break
                elif cons in self._constants_not_number.keys():
                    local_op_check = re.sub(r'\b'+cons+r'\b',' _ ',local_op_check)
                    found = True
                    break
            if not found:
                raise Exception(f'Constant {cons} not found')
            groups_re = re.findall(self._constant_name_re,local_op_check)
        self._equation_temp = local_op
        logger.debug(f' processed => {local_op}')

    def _eval_parentheses(self):
        logger.debug(f'Processing parentheses')
        local_op = self._equation_temp
        groups_re = re.findall(r'\([^\(\)]+\)',local_op)
        while len(groups_re)>0:
            for par in groups_re:
                logger.debug(f' ########################################## processing => {par}')
                partial = self._eval_equation(*self._eval_parse(par.replace('(','').replace(')','')))
                if type(partial) in [int, float]:
                    local_op = local_op.replace(par, str(partial))
                else:
                    # generate new constant
                    name = f'internal_{uuid.uuid1().hex}'
                    self._constants_not_number[name] = partial
                    local_op = local_op.replace(par, name)
                    logger.debug(f'Created {name} with value {partial}')
                logger.debug(f'      => {local_op}')
                groups_re = re.findall(r'\([^\(\)]+\)',local_op)
        self._equation_temp = local_op
        logger.debug(f'Final equation to eval => {self._equation_temp}')
        self._result = self._eval_equation(*self._eval_parse(self._equation_temp))

    def _eval_equation(self, nums, ops):
        logger.debug('_eval_equation function')
        logger.debug(f'Evaluating equation with => {nums} and {ops}')
        nums = list(nums)
        ops = list(ops)
        operator_order = ('^%','*/','+-')  #precedence from left to right.  operators at same index have same precendece.
        #map operators to functions.
        op_dict = {'^':operator.pow,
                   '%':operator.mod,
                   '*':operator.mul,
                   '/':operator.truediv,
                   '+':operator.add,
                   '-':operator.sub}
        Value = None
        for op in operator_order:                   #Loop over precedence levels
            logger.debug(f'Processing operator {op}...')
            while any(o in ops for o in op):        #Operator with this precedence level exists
                logger.debug('Found precedentce operator')
                idx,oo = next((i,o) for i,o in enumerate(ops) if o in op) #Next operator with this precedence
                ops.pop(idx)                        #remove this operator from the operator list
                logger.debug(f'Found nums and idx {nums} and {idx}')
                logger.debug(f'About to work with raw {nums[idx:idx+2]}')
                #values = map(float,nums[idx:idx+2]) #here I just assume float for everything
                values = list(map(lambda x: float(x) if (type(x) in [float, int]) or (type(x) == str and self._is_number(x)) else (self._constants_not_number[x.strip()] if type(x) == str else x), nums[idx:idx+2]))
                logger.debug(f'About to work with processed {list(values)}')
                value = op_dict[oo](*values)
                logger.debug(f'Got value {value}')
                nums[idx:idx+2] = [value]           #clear out those indices
                logger.debug(f'Final value {nums}')

        return nums[0]

    def _is_number(self, string):
        logger.debug('_is_number function')
        logger.debug(f'workign with {string}')
        logger.debug(type(string))
        try:
            # Check if the string is a valid integer
            int_value = int(string)
            logger.debug('is an int')
            return True
        except ValueError:
            try:
                # Check if the string is a valid float
                float_value = float(string)
                logger.debug('is a float')
                return True
            except ValueError:
                logger.debug('failure')
                return False

    def _eval_parse(self, x):
        logger.debug(f'Parsing equation with => {x}')

        operators = set('^%+-*/')

        op_out = []    #This holds the operators that are found in the string (left to right)
        num_out = []   #this holds the non-operators that are found in the string (left to right)
        buff = []
        for c in x:  #examine 1 character at a time
            if c in operators:
                #found an operator.  Everything we've accumulated in `buff` is
                #a single "number". Join it together and put it in `num_out`.
                num_out.append(''.join(buff))
                buff = []
                op_out.append(c)
            else:
                #not an operator.  Just accumulate this character in buff.
                buff.append(c)
        num_out.append(''.join(buff))
        logger.debug(f' answering parsing equation with => {num_out} and {op_out}')
        return num_out,op_out


if __name__ == "__main__":
    print('This script must be called as a module')
