#!/usr/bin/env python3

from distutils.core import setup

setup(
    name='kungfoo_simple_math_op_parser',
    version='0.0.8-dev',
    packages=['kungfoo_simple_math_op_parser',],
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('LongDescr.txt').read(),
    url='https://gitlab.com/juanmatias/kungfoo_simple_math_op_parser'
)
